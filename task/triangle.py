def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    if not all(isinstance(x, (int, float)) for x in (a, b, c)):
        return False
    if 0 in (a, b, c):
        return False
    if a + b <= c:
        return False
    elif a + c <= b:
        return False
    elif b + c <= a:
        return False
    return True


sides = (1, 2, 3)
print(is_triangle(*sides))
